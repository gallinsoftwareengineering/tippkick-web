package de.rgse.tippkick.web.composite;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import de.rgse.tippkick.model.RgseMatchdata;
import de.rgse.tippkick.service.IOpenLigaDbService;

@Named
public class MatchList {

	private List<RgseMatchdata> matches;
	private final IOpenLigaDbService openLigaDbService;

	@Inject
	public MatchList(final IOpenLigaDbService openLigaDbService) {
		this.openLigaDbService = openLigaDbService;
	}

	@PostConstruct
	private void postConstruct() {
		matches = openLigaDbService.getAllPendingMatchdata();
	}

	public List<RgseMatchdata> getMatches() {
		return matches;
	}
}
