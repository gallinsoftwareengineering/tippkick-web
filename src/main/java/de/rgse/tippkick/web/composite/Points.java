package de.rgse.tippkick.web.composite;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import de.rgse.tippkick.service.ReportService;
import de.rgse.tippkick.valueObjects.PlayerReport;

@Named
public class Points {

	@Inject
	private ReportService reportService;

	@Inject
	private String currentUserId;

	private PlayerReport reportForUser;

	@PostConstruct
	private void postConstruct() {
		reportForUser = reportService.getReportForUser(currentUserId);
	}

	public PlayerReport getReportForUser() {
		return reportForUser;
	}
}
