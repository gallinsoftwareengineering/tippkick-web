package de.rgse.tippkick.web.composite;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import de.rgse.tippkick.model.Guess;
import de.rgse.tippkick.model.RgseMatchdata;
import de.rgse.tippkick.service.GuessDao;
import de.rgse.tippkick.service.IOpenLigaDbService;

@Named
@ViewScoped
@SuppressWarnings("serial")
public class MatchDetail implements Serializable {

    @Inject
    private IOpenLigaDbService openLigaDbService;
    @Inject
    private GuessDao guessDao;
    @Inject
    private String currentUserId;

    private RgseMatchdata matchdata;
    private List<Guess> guesses;

    private Guess myGuess;

    public MatchDetail() {
    }

    @PostConstruct
    private void postConstruct() {
        final String matchId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
                .get("id");
        matchdata = openLigaDbService.getMatchdata(matchId);
        guesses = guessDao.findByMatchData(matchdata.getMatchID(), currentUserId);
        myGuess = guessDao.findByMatchAndUser(matchId, currentUserId);

        if (myGuess == null) {
            myGuess = new Guess(matchId, currentUserId);
        }

    }

    public RgseMatchdata getMatchdata() {
        return matchdata;
    }

    public List<Guess> getGuesses() {
        return guesses;
    }

    public void addTipp() {
        guessDao.update(myGuess);
    }

    public Guess getMyGuess() {
        return myGuess;
    }
}
