package de.rgse.tippkick.web.composite;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import de.rgse.tippkick.service.ReportService;
import de.rgse.tippkick.valueObjects.PlayerReport;

@Named
public class Overview {

	@Inject
	private ReportService reportService;
	private List<PlayerReport> playerReports;

	@PostConstruct
	private void postConstruct() {
		playerReports = reportService.getPlayerReports();
	}

	public List<PlayerReport> getPlayerReports() {
		return playerReports;
	}
	
	
}
