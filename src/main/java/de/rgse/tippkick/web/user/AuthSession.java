package de.rgse.tippkick.web.user;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.api.ApiKey;
import com.stormpath.sdk.api.ApiKeys;
import com.stormpath.sdk.application.Application;
import com.stormpath.sdk.client.Client;
import com.stormpath.sdk.client.Clients;
import com.stormpath.sdk.client.Proxy;

@Named
@SessionScoped
@SuppressWarnings("serial")
public class AuthSession implements Serializable {

    private Client client;

    private Application application;

    @PostConstruct
    private void postConstruct() {
        try {
            final ApiKey apiKey = getApiKey();
            final Proxy proxy = new Proxy("proxy.berlinwasser.de", 8541);
            client = Clients.builder().setApiKey(apiKey).setProxy(proxy).build();

            application = client.getResource("https://api.stormpath.com/v1/applications/79BK9dSlHdBeQBVZ8uFwUR",
                    Application.class);

        } catch (final IOException e) {
            throw new RuntimeException("unable to read auth settings", e);
        }
    }

    public Account getUser(String id) {
        final Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("username", id);
        return application.getAccounts(queryParams).single();
    }

    public Account getCurrentUser() {
        final Subject subject = SecurityUtils.getSubject();

        Account account = null;
        if (subject.isAuthenticated()) {
            account = client.getResource(subject.getPrincipal().toString(), Account.class);
        }

        return account;
    }

    public void logout() throws IOException {
        SecurityUtils.getSubject().logout();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        FacesContext.getCurrentInstance().getExternalContext().redirect("index.jsf");
    }

    private ApiKey getApiKey() throws IOException {
        final Properties properties = new Properties();

        try (InputStream inputStream = getClass().getResourceAsStream("/apiKey.properties")) {
            properties.load(inputStream);
        }

        final String apiKeyId = properties.getProperty("apiKey.id");
        final String apiKeySecret = properties.getProperty("apiKey.secret");

        return ApiKeys.builder().setId(apiKeyId).setSecret(apiKeySecret).build();
    }

    @Named("currentUserId")
    @Produces
    public String getCurrentUserId() {
        final Account currentUser = getCurrentUser();
        return null == currentUser ? "" : currentUser.getUsername();
    }
}
