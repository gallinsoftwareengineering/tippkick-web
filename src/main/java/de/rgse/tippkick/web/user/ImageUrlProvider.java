package de.rgse.tippkick.web.user;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import com.stormpath.sdk.account.Account;

@Named
@SessionScoped
@SuppressWarnings("serial")
public class ImageUrlProvider implements Serializable {

    private static final String BASE_URL = "https://secure.gravatar.com/avatar/%s?d=identicon";

    private final MessageDigest digest;
    private final HexBinaryAdapter adapter;

    private Map<String, String> cache;

    @Inject
    private AuthSession authSession;

    public ImageUrlProvider() {
        try {
            digest = MessageDigest.getInstance("MD5");
            adapter = new HexBinaryAdapter();

            cache = new HashMap<>();

        } catch (final NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public String getGravatarUrl(String userId) {
        String result = cache.get(userId);

        if (result == null) {
            final Account user = authSession.getUser(userId);

            final String hex = adapter.marshal(digest.digest(user.getEmail().getBytes()));
            result = String.format(BASE_URL, hex.toLowerCase());

            cache.put(userId, result);
        }

        return result;
    }
}
