package de.rgse.tippkick.web.user;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.stormpath.sdk.client.Client;
import com.stormpath.sdk.client.Proxy;

public class ClientBuilderFactory extends com.stormpath.shiro.client.ClientFactory {

    private Proxy proxy;

    public ClientBuilderFactory() {
        System.out.println("costum ClientBuilderFactory initialised");
        setProxyFromSystem();
        setAlternateProxyFromSettings();
    }

    @Override
    protected Client createInstance() {
        System.out.println("costum ClientBuilderFactory.createInstance()");

        Client result = null;
        if (proxy != null) {
            result = super.getClientBuilder().setProxy(proxy).build();

        } else {
            result = super.getClientBuilder().build();

        }

        return result;

    }

    private void setProxyFromSystem() {
        final String httpHost = System.getProperty("http.proxyHost");
        if (httpHost != null) {
            final int httpPort = Integer.valueOf(System.getProperty("http.proxyPort"));
            proxy = new Proxy(httpHost, httpPort);
        }
    }

    private void setAlternateProxyFromSettings() {
        if (proxy == null) {
            final Properties properties = new Properties();
            try (InputStream inputStream = getClass().getResourceAsStream("/settings.properties")) {
                properties.load(inputStream);

                final String httpHost = properties.getProperty("http.proxyHost");

                if (httpHost != null) {
                    final int httpPort = Integer.valueOf(properties.getProperty("http.proxyPort"));
                    proxy = new Proxy(httpHost, httpPort);
                }

            } catch (final IOException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "unable to initialise proxy settings", e);
            }
        }

    }

}
